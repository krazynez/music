# Setup

1.	Go [here](https://github.com/jaseg/python-mpv) and make sure you have the correct dependencies for python-mpv

2.	`pip install -r requirements.txt`


# Running

1.	`./main.py` 

2. `./main -h <to see supported flags>`


##### *NOTES*:

For backgrounding currently on Linux (possibly Mac) you have to do `nohup ./main.py` or `./main.py && disown -h`

<hr>

**WINDOWS USERS**:

READ libmpv section **CAREFULLY** You have to have make sure the `mpv-1.dll` 
<br> is in your **SYSTEM** `%PATH%` **not** the Users `%PATH%`
