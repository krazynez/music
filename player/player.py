#!/usr/bin/env python3

import mpv

class Music:
    def __init__(self, minimized=False, video=False ):
        # Working on Backgrounding somehow /start maybe? (Windows)
        if minimized: 
            self.player = mpv.MPV(ytdl=True, window_minimized=True)
        elif not minimized and video is True:
            self.player = mpv.MPV(ytdl=True) 
        else:
            self.player = mpv.MPV(ytdl=True, video=False) 

        self.player.play("https://www.youtube.com/watch?v=bmVKaAV_7-A")
        print("\nPlaying...\n")
        self.player.wait_for_playback()

        del self.player
