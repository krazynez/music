#!/usr/bin/env python3


import mpv, platform
from player.player import Music
import argparse

def main(m, v):
    music = Music(m, v)   

if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(description="Flags to send to customize your music player")
        parser.add_argument("-vid", "--video", action="store_true")
        parser.add_argument("-m", "--minimized", action="store_true")
        args = parser.parse_args()
        main(args.minimized, args.video)
    except KeyboardInterrupt:
        print("User Quit...\n")
        exit()

